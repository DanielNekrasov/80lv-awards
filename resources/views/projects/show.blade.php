@extends('layouts.master')
@section('og-image', empty(json_decode($project->images)) ? Voyager::image($project->thumbnail) : Voyager::image(json_decode($project->images)[0]))
@section('title', $project->author)

@section('content')
    <div class="page-wrapper awards-section">
        <div class="page-content page-detail">
            <div class="art-panel">
                <div class="art-panel__body">
                    @if($project->video)
                        @php
                            $video = $project->getVideoSrc();
                        @endphp
                        <div class="video-container">
                            @if($video['source'] == 'vimeo')
                                <iframe src="https://player.vimeo.com/video/{{$video['url']}}?autoplay=1&loop=1&autopause=0&muted=1&fullscreen=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            @elseif($video['source'] == 'youtube')
                                <iframe id="ytplayer" type="text/html" width="100%" height="100%"
                                        src="https://www.youtube.com/embed/{{$video['url']}}?enablejsapi=1&autoplay=1&controls=0&showinfo=0&rel=0&loop=1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen frameborder="0"></iframe>
                            @endif
                            <div class="favorite">
                                <span class="favorite__icon"></span>
                            </div>
                        </div>
                    @else
                        <div class="gallery-slider">
                            <div class="slider gallery-slider__main">
                                <div class="slider__wrapper">
                                    @php
                                        $images = json_decode($project->images)
                                    @endphp
                                    @if ($images)
                                        @foreach($images as $image)
                                            <div class="slider__slide swiper-lazy" data-background="{{$img_adapt ? Voyager::image($project->getThumbnail($image,'medium')) : Voyager::image($image)}}">
                                                <div class="slider__preloader"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="slider__pagination"></div>
                                <div class="favorite">
                                    <span class="favorite__icon"></span>
                                </div>
                            </div>
                            <div class="slider gallery-slider__thumbs d-none d-sm-block">
                                <div class="slider__wrapper">
                                    @if ($images)
                                        @foreach($images as $image)
                                            <div class="slider__slide swiper-lazy" data-background="{{Voyager::image($project->getThumbnail($image,'small'))}}">
                                                <div class="slider__preloader"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($project->concept_by)
                        <div class="art-panel__label d-none d-sm-block">
                            Concept by {{$project->concept_by}}
                        </div>
                    @endif
                    <a href="{{$project->article_link}}" class="art-panel__beauty d-none d-sm-inline-block" target="_blank" rel="noopener">
                        <div class="ribbon">
                            <span class="ribbon__text">Read about on</span>
                            <!--                            <span class="ribbon__text">1<sup>st</sup> place</span>-->
                            <!--                            <span class="ribbon__text">2<sup>nd</sup> place</span>-->
                            <!--                            <span class="ribbon__text">3<sup>rd</sup> place</span>-->
                        </div>
                    </a>
                </div>
                <div class="art-info art-panel__footer">
                    <div class="art-info__description">
                        <div class="art-info__title-wrap">
                            <h4 class="art-info__title">{{$project->author}}</h4>
                            @if($project->artstation || $project->instragram || $project->facebook || $project->twitter || $project->linkedin)
                                <ul class="social-list">
                                    @isset($project->artstation)
                                        <li class="social-list__item">
                                            <a href="{{$project->artstation}}" target="_blank" rel="noopener"><span class="icon-social icon-social_triangle"></span></a>
                                        </li>
                                    @endisset
                                    @isset($project->instragram)
                                        <li class="social-list__item">
                                            <a href="{{$project->instragram}}" target="_blank" rel="noopener"><span class="icon-social icon-social_instagram"></span></a>
                                        </li>
                                    @endisset
                                    @isset($project->facebook)
                                        <li class="social-list__item">
                                            <a href="{{$project->facebook}}" target="_blank" rel="noopener"><span class="icon-social icon-social_fb"></span></a>
                                        </li>
                                    @endisset
                                    @isset($project->twitter)
                                        <li class="social-list__item">
                                            <a href="{{$project->twitter}}" target="_blank" rel="noopener"><span class="icon-social icon-social_twitter"></span></a>
                                        </li>
                                    @endisset
                                    @isset($project->linkedin)
                                        <li class="social-list__item">
                                            <a href="{{$project->linkedin}}" target="_blank" rel="noopener"><span class="icon-social icon-social_in"></span></a>
                                        </li>
                                    @endisset
                                </ul>
                            @endif
                        </div>
                        @if($project->vendors()->count() > 0)
                            <div class="art-info__vendors d-none d-md-block">
                                Vendors:
                                @foreach($project->vendors as $vendor)
                                    <a href="{{$vendor->link}}" class="art-info__vendor-link" target="_blank" rel="noopener">{{$vendor->name}}@if (!$loop->last),@endif</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="art-info__interaction">
                        <div class="art-info__btn-wrap">
                            <div class="art-info__choose-wrapper">
                                @if (Auth::guest())
                                    <button class="btn btn_dark btn_tight btn_disabled art-info__btn-choose" disabled="disabled">
                                        <svg version="1.1" class="btn__icon icon icon-goblet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 73 72" enable-background="new 0 0 73 72" xml:space="preserve">
                                            <path fill="#E4C291" class="icon-svg" d="M69.3,7.2H58.5V3.6c0-2-1.6-3.6-3.6-3.6h-36c-2,0-3.6,1.6-3.6,3.6l0,3.6H4.5c-2,0-3.6,1.6-3.6,3.6v10.8
                                                C0.9,29.5,7.3,36,15.3,36h0.2c1.6,9.1,8.7,16.2,17.8,17.7v11.1H22.5V72h28.8v-7.2H40.5V53.7c9-1.5,16.1-8.7,17.7-17.7h0.3
                                                c7.9,0,14.4-6.5,14.4-14.4V10.8C72.9,8.8,71.3,7.2,69.3,7.2z M8.1,21.6v-7.2h7.2l-0.1,14.4C11.3,28.7,8.1,25.5,8.1,21.6z M46.5,36
                                                l-9.6-6.4L27.3,36l3.2-11.2l-8-6.8l9.6-1.2l4.8-9.6l4.8,9.6l9.6,1.2l-8,6.8L46.5,36z M58.5,28.8V14.4h7.2v7.2
                                                C65.7,25.6,62.4,28.8,58.5,28.8z"/>
                                        </svg>
                                        <span class="btn__text">Choose</span>
                                    </button>
                                    <div class="art-info__hint">
                                        Please <a href="{{ url('login/facebook') }}" title="Sign In">Sign In</a> to vote
                                    </div>
                                @else
                                    <form method="POST" action="/projects/{{ $project->id }}/favorites" id="favorites">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn_dark btn_tight art-info__btn-choose {{$project->isFavorited() ? 'btn_active':''}}"
                                                {{$project->isFavorited() ? 'disabled="disabled"' :'' }}>
                                            <svg version="1.1" class="btn__icon icon icon-goblet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 73 72" enable-background="new 0 0 73 72" xml:space="preserve">
                                                <path fill="#E4C291" class="icon-svg" d="M69.3,7.2H58.5V3.6c0-2-1.6-3.6-3.6-3.6h-36c-2,0-3.6,1.6-3.6,3.6l0,3.6H4.5c-2,0-3.6,1.6-3.6,3.6v10.8
                                                    C0.9,29.5,7.3,36,15.3,36h0.2c1.6,9.1,8.7,16.2,17.8,17.7v11.1H22.5V72h28.8v-7.2H40.5V53.7c9-1.5,16.1-8.7,17.7-17.7h0.3
                                                    c7.9,0,14.4-6.5,14.4-14.4V10.8C72.9,8.8,71.3,7.2,69.3,7.2z M8.1,21.6v-7.2h7.2l-0.1,14.4C11.3,28.7,8.1,25.5,8.1,21.6z M46.5,36
                                                    l-9.6-6.4L27.3,36l3.2-11.2l-8-6.8l9.6-1.2l4.8-9.6l4.8,9.6l9.6,1.2l-8,6.8L46.5,36z M58.5,28.8V14.4h7.2v7.2
                                                    C65.7,25.6,62.4,28.8,58.5,28.8z"/>
                                            </svg>
                                            <span class="btn__text">{{$project->isFavorited() ? 'This is my choice':'Choose'}}</span>
                                        </button>
                                    </form>
                                @endif
                            </div>
                            <div class="art-info__btn-row d-none d-sm-flex">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" class="btn btn_dark btn_tight share-button" aria-label="share on facebook">
                                    <span class="icon icon-fb icon-fb_bigger"></span>
                                </a>
                                <a href="https://twitter.com/intent/tweet?url={{url()->current()}}" class="btn btn_dark btn_tight tw-share" aria-label="share on twitter">
                                    <span class="icon icon-twitter icon-twitter_bigger"></span>
                                </a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&source={{url()->current()}}" class="btn btn_dark btn_tight share-button" aria-label="share on linkedin">
                                    <span class="icon icon-linked icon-linked_bigger"></span>
                                </a>
                            </div>
                        </div>
                        <div class="art-info__btn-row art-info__controls">
                            <a href="{{route('projects', $previous->getLink(\Request::has('favorited')))}}" class="btn_expanded art-info__btn-left" data-control="prev">
                                <div class="btn btn_dark btn_tight">
                                    <span class="btn__icon icon icon-left"></span><span class="btn__text">Previous work</span>
                                </div>
                            </a>
                            <a href="{{route('projects', $next->getLink(\Request::has('favorited')))}}" class="btn_expanded art-info__btn-right" data-control="next">
                                <div class="btn btn_dark btn_tight">
                                    <span class="btn__text">Next work</span><span class="btn__icon icon icon-right"></span>
                                </div>
                            </a>
                            <a href="/#{{\Request::has('favorited') ? "your-choice" : $project->category->slug}}" class="btn_expanded art-info__btn-cross" data-control="close">
                                <div class="btn btn_dark btn_tight">
                                    <span class="btn__text">Close</span><span class="btn__icon icon icon-cross"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.modal')

    <!-- Scripts -->
    @if($project->video)
        <script async src="https://www.youtube.com/iframe_api"></script>
        <script>
          function onYouTubeIframeAPIReady() {
            var player;
            player = new YT.Player('ytplayer', {
              events: {
                onReady: function(e) {
                  e.target.mute();
                }
              }
            });
          }
        </script>
    @endif
@endsection