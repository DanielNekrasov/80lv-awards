<div class="page-footer">
    <footer class="footer container-fluid clear-padding-left">
        <div class="footer__social">
            <span>Share contest</span>
            <ul class="nav footer__nav">
                <li class="nav__item">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" class="share-button" aria-label="share on facebook">
                        <span class="icon icon_zoomed icon-fb"></span>
                    </a>
                </li>
                <li class="nav__item">
                    <a href="https://twitter.com/intent/tweet?url={{url()->current()}}" class="tw-share" aria-label="share on twitter">
                        <span class="icon icon_zoomed icon-twitter"></span>
                    </a>
                </li>
                <li class="nav__item">
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&source={{url()->current()}}" class="share-button" aria-label="share on linkedin">
                        <span class="icon icon_zoomed icon-linked"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footer__copyright">
            Created by <a href="https://rktv.ru/" target="_blank" rel="noopener">Reactive Media</a>
        </div>
    </footer>
</div>