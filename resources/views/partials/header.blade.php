<div class="page-header">
    <header class="header">
        <div class="header__wrap container-fluid">
            <div class="logo">
                <img src="/pics/logo.svg" alt="80 level">
            </div>

            <div class="nav-main">
                <ul class="nav nav-main__nav">
                    <li class="nav__item">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" class="share-button" aria-label="share on facebook">
                            <span class="icon icon-fb icon_zoomed"></span>
                        </a>
                    </li>
                    <li class="nav__item">
                        <a href="https://twitter.com/intent/tweet?url={{url()->current()}}" class="share-button tw-share" aria-label="share on twitter">
                            <span class="icon icon-twitter icon_zoomed"></span>
                        </a>
                    </li>
                    <li class="nav__item">
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&source={{url()->current()}}" class="share-button" aria-label="share on linkedin">
                            <span class="icon icon-linked icon_zoomed"></span>
                        </a>
                    </li>
                    @if (Auth::check())
                        <li class="nav__item d-none d-md-inline-block">
                            <span>{{ Auth::user()->name }}</span>
                        </li>
                        <li class="nav__item d-md-none">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="icon icon-logout icon_zoomed"></span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @else
                        <li class="nav__item d-md-none">
                            <a href="{{ url('login/facebook') }}"><span class="icon icon-user icon_zoomed"></span></a>
                        </li>
                    @endif
                </ul>
                @if (Auth::guest())
                    <a href="{{ url('login/facebook') }}" class="btn btn_zoomed d-none d-md-inline-block" title="Sign In">Sign In</a>
                @else
                    <a href="{{ route('logout') }}" class="btn btn_zoomed d-none d-md-inline-block" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="Log out">Log out</a>
                @endif
            </div>
        </div>
    </header>
</div>