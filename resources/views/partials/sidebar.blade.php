<aside class="sidebar {{\Request::is('projects/*') ? "sidebar_show" : "" }} container-fluid ">
	<ul class="nav-circles sidebar__nav">
		<li class="nav-circles__item">
			<a href=/#about><span>About</span></a>
		</li>
		@foreach ($categories as $key => $category)
			<li class="nav-circles__item {{(Route::currentRouteName() == "projects" && $project->category_id == $category->id && !\Request::has('favorited')) ? "nav-circles__item_active": ""}}">
				<a href="/#{{$category->slug}}"><span>{{ $category->name }}</span></a>
			</li>
		@endforeach

		@if($user = Auth::user())
			@if($user->hasFavorites())
				<li class="nav-circles__item {{\Request::has('favorited') ? "nav-circles__item_active" : ""}}">
					<a href="/#your-choice"><span>Your choice</span></a>
				</li>
			@endif
		@endif
	</ul>
	@if ($side_banner && \Storage::disk('public')->exists($side_banner->source))
		<div class="sidebar__banner banner banner_side d-none d-md-inline-block">
			@if(\Storage::disk('public')->mimeType($side_banner->source) == 'video/mp4')
				<a href="{{$side_banner->link}}" target="_blank" rel="noopener" class="banner__inner">
					<video src="{{\Storage::disk('public')->url($side_banner->source)}}" muted loop autoplay></video>
				</a>
			@else
				<a href="{{$side_banner->link}}" target="_blank" rel="noopener" class="banner__inner" style="background-image: url({{\Storage::disk('public')->url($side_banner->source)}})"></a>
			@endif
		</div>
	@endif
	<div class="progress-bar sidebar__progress-bar d-none d-md-flex" data-step="{{$progressBar['step']}}">
		<span class="progress-bar__label">{{$progressBar['step'] != 5 ? 'Make choice in all categories to finish' : 'You are a Superman!'}}</span>
        <div class="d-flex">
            <div class="progress-bar__bar">
                <video class="active" src="{{$progressBar['src']}}" muted autoplay ></video>
			</div>
        </div>
	</div>
</aside>

