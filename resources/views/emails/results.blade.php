@component('mail::message')

The results of 80 Level Awards have finally been revealed! Follow <a href="{{env('APP_URL')}}" target="_blank" rel="noopener">this link</a> to learn about the winners in each category.

The energy in this competition has been truly extraordinary! We were blown away by the number of participants and we want to thank every single one of you for your interest and time. You all made this event more exciting.

See you next year!

@endcomponent
