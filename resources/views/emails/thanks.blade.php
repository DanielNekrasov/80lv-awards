@component('mail::message')

Thank you for taking part in 80 Level Awards. Your choices will help us define the best works of 2018 and we truly appreciate that. The results will be announced on <b>January 15th</b>, so don’t forget to come back!

Please help us spread the word by sharing links on social media. It would help us evaluate more opinions to choose the winners.

@endcomponent
