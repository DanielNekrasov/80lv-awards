@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.'Favorites')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-pirate-swords"></i> Rating
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Project</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $project)
                                        <tr>
                                            <td>{{$project['id']}}</td>
                                            <td>{{$project['name']}}</td>
                                            <td>{{$project['author']}}</td>
                                            <td>{{$project['category']}}</td>
                                            <td>{{$project['total']}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
