@extends('layouts.master')

@section('page-class', 'categories-page')

@section('content')

<div class="preloader">
    <div class="preloader__wrap"><img id="loader-logo" src="pics/Logo-big.svg">
        <div class="preloader__empty-line">
            <div class="preloader__full-line"></div>
        </div>
    </div>
</div>

<div class="fullpage-slider">
    <section class="page-wrapper welcome-section">
        @include('partials.header')
        <div class="page-content">
            <div class="slider slider-main">
                <div class="slider__wrapper">
                    <div class="slider__slide">
                        <div class="fullscreen-video">
                            <video muted preload="metadata" autoplay loop="loop" class="startVideo" poster="/pics/slides/video-poster.jpg">
                                <source src="/video/caring_animation.mp4" type='video/mp4'>
                                <source src="/video/caring_animation.webm" type='video/webm'>
                            </video>
                        </div>
                    </div>
                </div>
            </div>

            <div class="welcome-section__content container-fluid">
                <div class="welcome-section__text-faded transform-effect transform-effect_left">80 level</div>
                <div class="welcome-section__text">
                    <h1 class="welcome-section__title transform-effect transform-effect_right">Awards 2018</h1>
                    <h2 class="welcome-section__subtitle transform-effect transform-effect_fadeIn">Choose one favourite artwork
                        in every category</h2>
                    {{--<h2 class="welcome-section__subtitle">Contest is over. We will announce the results on <b>December 1</b>.</h2>--}}
                    <div class="welcome-section__timer timer transform-effect transform-effect_fadeIn" data-award="Jan 5, 2019 00:00:00"></div>
                </div>
                <div class="welcome-section__cover-rights transform-effect transform-effect_right">
                    Cover art by <a href="https://www.artstation.com/yarrid" target="_blank" rel="noopener">Yarrid Henrard</a>
                </div>
            </div>

            <div class="scroll-tip d-none d-md-block transform-effect transform-effect_up">
                Scroll to vote
            </div>
        </div>
    </section>
    <section class="page-wrapper awards-section" data-anchor="about">
        <div class="page-content">
            <div class="container-fluid content-wrap">
                <div class="row">
                    <div class="col-12 col-sm-10 col-md-8">
                        <div class="content">
                            <h3>We invite you all to take a part in the annual 80 Level Awards</h3>
                            <p>Artists have been working hard the whole year creating outstanding works and sharing their experience and knowledge with the community. </p>
                            <p>80 Level Awards is a way to say thank you to all those talents who presented outstanding environments, set up mind-blowing simulations, built graphs for intricate materials, modeled detailed props, and sculpted beautiful characters.</p>
                            <p>You pick one favorite in each category and we’ll define the best of the best in a month! The winners, defined by the community, will get some cool prizes!</p>
                        </div>
                        <div class="banners d-none d-md-block">
                            <div class="row">
                                <div class="banner col">
                                    <a href="https://www.cgmasteracademy.com/" target="_blank" rel="noopener" class="banner__inner" style="background-image: url('/pics/banners/cgma_banner.png')"></a>
                                </div>
                                <div class="banner col">
                                    <a href="https://www.sidefx.com/" target="_blank" rel="noopener" class="banner__inner" style="background-image: url('/pics/banners/sfx_banner.png')"></a>
                                </div>
                                <div class="banner col">
                                    <a href="https://www.quixel.com" target="_blank" rel="noopener" class="banner__inner" style="background-image: url('/pics/banners/megascans_banner.png')"></a>
                                </div>
                                <div class="banner col">
                                    <a href="https://www.allegorithmic.com/" target="_blank" rel="noopener" class="banner__inner" style="background-image: url('/pics/banners/substance_white.png?v=2')"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="decoration d-none d-md-inline-block">
                <img src="/pics/rock-and-roll.svg" alt="decoration">
            </div>
        </div>
    </section>

    @foreach ($categories as $key => $category)
        <section class="page-wrapper awards-section awards-section_gallery" data-anchor="{{$category->slug}}">
            <div class="page-content gallery-page">
                <div class="gallery container-fluid clear-intend-right">
                   @foreach($category->projects as $project)
                        <a href="{{ route('projects', ['project' => $project->slug], false)}}" class="gallery__item card-art {{$project->isFavorited() ? "card-art_active" : ""}}" style="background-image: url({{ Voyager::image($project->thumbnail) }})">
                            <div class="card-art__inner">
                                <span class="card-art__title">{{$project->author}}</span>
                            </div>
                            <div class="card-art__chosen">
                                <div class="card-art__chosen-wrap">
                                    <span>This is my choice</span>
                                </div>
                            </div>
                        </a>
                    @endforeach
                    @if(count($category->projects) < 20)
                        @for($i=0; $i < 20-count($category->projects); $i++)
                            <div class="gallery__item card-art card-art_empty">
                                <span>Awards<br>2018</span>
                            </div>
                       @endfor
                    @endif
                </div>
            </div>
        </section>
    @endforeach

    @if($favorited_projects)
        <section class="page-wrapper awards-section awards-section_choice" data-anchor="your-choice">
            <div class="page-content gallery-page">
                <div class="container-fluid content-wrap">
                    <div class="row">
                        <div class="col">
                            <div class="content">
                                <h3>your choice</h3>
                                <p>Thanks for participation. We hope you had fun. We will announce the results on <b>January 15.</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gallery container-fluid gallery_limit">
                    @foreach($favorited_projects as $project)
                        <a href="{{ route('projects', ['project' => $project->slug, 'favorited' => 1], false)}}" class="gallery__item card-art" style="background-image: url({{ Voyager::image($project->thumbnail) }})">
                            <div class="card-art__inner">
                                <span class="card-art__title">{{$project->author}}</span>
                            </div>
                        </a>
                    @endforeach
                </div>

                @if ($across_banner && \Storage::disk('public')->exists($across_banner->source))
                    <div class="banner banner_across container-fluid d-none d-md-block">
                        @if(\Storage::disk('public')->mimeType($across_banner->source) == 'video/mp4')
                            <a href="{{$across_banner->link}}" target="_blank" rel="noopener" class="banner__inner">
                                <video src="{{\Storage::disk('public')->url($across_banner->source)}}" muted loop autoplay></video>
                            </a>
                        @else
                            <a href="{{$across_banner->link}}" target="_blank" rel="noopener" class="banner__inner" style="background-image: url({{\Storage::disk('public')->url($across_banner->source)}})"></a>
                        @endif
                    </div>
                @endif
            </div>
            @include('partials.footer')
        </section>
    @endif
</div>

@endsection
