<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="ru" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="ru" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="ru" class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html lang="ru" class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', setting('site.title'))</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="{{setting('site.description')}}">
    <meta property="og:site_name" content="{{ setting('site.title') }}" />
    <meta property="og:locale" content="en_US" />

    <meta property="fb:app_id" content="{{env('FACEBOOK_ID')}}" />
    <meta property="og:title" content="@yield('title', setting('site.title'))" />
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ Request::root() }}@yield('og-image', '/pics/favicon/facebook.jpg')" />
    <meta property="og:image:width" content="1920" />
    <meta property="og:image:height" content="1080" />
    <meta property="og:description" content="{{setting('site.description')}}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="@yield('title', setting('site.title'))" />
    <meta name="twitter:description" content="{{setting('site.description')}}" />
    <meta name="twitter:image" content="{{ Request::root() }}@yield('og-image', '/pics/favicon/facebook.jpg')" />

    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="format-detection" content="telephone=no">

    <!-- Styles -->
    <link href="{{ mix('css/main.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/pics/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/pics/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/pics/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/pics/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/pics/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/pics/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/pics/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/pics/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="{{ config('app.name', '80 level') }}"/>
    <meta name="msapplication-TileColor" content="#141011" />
    <meta name="msapplication-TileImage" content="/pics/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/pics/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/pics/favicon/mstile-150x150.png" />

    <meta name="theme-color" content="#141011">

    <link rel="manifest" href="/manifest.webmanifest">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{setting('site.google_analytics_tracking_id')}}"></script>
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', '{{setting('site.google_analytics_tracking_id')}}');
    </script>
</head>
<body>
<!--[if lt IE 10]>
<div class="browsehappy">
    <div class="center">
        <p class="chromeframe">Ваш браузер <strong>давно устарел</strong>. Пожалуйста <a href="http://browsehappy.com/">обновите браузер</a> или <a href="http://www.google.com/chrome/">установите Google Chrome</a> для комфортной работы с этим сайтом.</p>
    </div>
</div>
<![endif]-->

    <div class="page-wrapper @yield('page-class')">
        @include('partials.sidebar')

        @yield('content')
    </div>

    <script src="{{  mix('js/main.js') }}"></script>
</body>
</html>
