<aside class="sidebar container-fluid">
	<ul class="nav-circles sidebar__nav">
		<li class="nav-circles__item">
			<a href="" data-index="2"><span>About</span></a>
		</li>
		<li class="nav-circles__item">
			<a href="" data-index="3"><span>Character Art</span></a>
		</li>
		<li class="nav-circles__item">
			<a href="" data-index="4"><span>Animation</span></a>
		</li>
		<li class="nav-circles__item">
			<a href="" data-index="5"><span>Environment</span></a>
		</li>
		<li class="nav-circles__item">
			<a href="" data-index="6"><span>Pixel Art</span></a>
		</li>
		<li class="nav-circles__item">
			<a href="" data-index="7"><span>CGI</span></a>
		</li>
	</ul>
	<div class="progress-bar sidebar__progress-bar d-none d-md-flex">
<!--		<input type="button" id="next" value="NEXT" style="color: black">-->
		<span class="progress-bar__label">Make choice in Animation, Environment Art, Pixel Art, CGI to finish</span>

        <div class="d-flex">
            <div class="progress-bar__bar">
                <video class="active" muted src="/video/walking-in-bar.mp4"></video>
            </div>
        </div>
	</div>

</aside>