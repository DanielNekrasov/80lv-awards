<div class="modal">
    <div class="modal__content">
        <div class="modal__instruction-row">
            <p class="modal__text d-none d-md-block">Use Left & Right keys to switch artworks</p>
            <p class="modal__text d-md-none">Use Left & Right swipes to switch artworks </p>
            <img src="/pics/left-right.svg" class="modal__arrows-horizontal" alt="">
        </div>

        <div class="modal__instruction-row">
            <p class="modal__text d-none d-md-block">Scroll to switch the pictures inside the artwork</p>
            <p class="modal__text d-md-none">  Use Up & Down swipes to switch the pictures inside the artwork</p>
            <img src="/pics/up-down.svg" class="modal__arrows-vertical" alt="">
        </div>

        <div class="modal__instruction-row">
            <p class="modal__text d-none d-md-block">Use Double-click to choose artwork as favourite</p>
            <p class="modal__text d-md-none">Use Double-tap to choose artwork as favourite</p>
            <span class="modal__double-circle">2</span>
        </div>

        <button class="btn modal__btn" id="modal-close">Got it</button>
    </div>
</div>