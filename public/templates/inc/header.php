<div class="page-header">
    <header class="header">
        <div class="header__wrap container-fluid">
            <div class="logo">
                <img src="/pics/logo.svg" alt="80 level">
            </div>

            <div class="nav-main">
                <ul class="nav nav-main__nav">
                    <li class="nav__item">
                        <a href="#"><span class="icon icon-fb"></span></a>
                    </li>
                    <li class="nav__item">
                        <a href="#"><span class="icon icon-twitter"></span></a>
                    </li>
                    <li class="nav__item">
                        <a href="#"><span class="icon icon-google"></span></a>
                    </li>
                    <li class="nav__item d-none d-md-inline-block">
                        <span class="">Bill Denbrough</span>
                    </li>
                    <li class="nav__item d-md-none">
                        <a href="#"><span class="icon icon-user"></span></a>
                    </li>
                    <li class="nav__item d-md-none">
                        <a href="#"><span class="icon icon-logout"></span></a>
                    </li>
                </ul>

                <a href="/" class="btn d-none d-md-inline-block" title="Sign In">Sign In</a>
            </div>
        </div>
    </header>
</div>