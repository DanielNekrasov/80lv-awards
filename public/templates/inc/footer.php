<div class="page-footer">
    <footer class="footer container-fluid clear-padding-left">
        <div class="footer__social">
            <span>Share contest</span>
            <ul class="nav footer__nav">
                <li class="nav__item">
                    <a href="#"><span class="icon icon-fb"></span></a>
                </li>
                <li class="nav__item">
                    <a href="#"><span class="icon icon-twitter"></span></a>
                </li>
                <li class="nav__item">
                    <a href="#"><span class="icon icon-google"></span></a>
                </li>
            </ul>
        </div>
        <div class="footer__copyright">
            Created by <a href="https://rktv.ru/" target="_blank" rel="nofollow">Reactive Media</a>
        </div>
    </footer>
</div>