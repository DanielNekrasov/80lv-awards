<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/meta.php";?>

<div class="page-wrapper">
	<?php include $_SERVER['DOCUMENT_ROOT']."/templates/partial/sidebar.php";?>

    <div class="fullpage-slider">
        <section class="page-wrapper welcome-section">
            <?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/header.php";?>
            <div class="page-content">
                <div class="slider slider-main">
                    <div class="slider__parallax-bg" style="background-image:url()" data-swiper-parallax="-23%"></div>
                    <div class="slider__wrapper">
                        <div class="slider__slide"  style="background-image: url('/pics/slides/1.png');">
                        </div>
                        <div class="slider__slide"  style="background-image: url('/pics/slides/1.png');">
                        </div>
                    </div>
                </div>

                <div class="welcome-section__content container-fluid">
                    <div class="welcome-section__text-faded">80 level</div>
                    <div class="welcome-section__text">
                        <h1 class="welcome-section__title">Awards 2018</h1>
<!--                        <h2 class="welcome-section__subtitle">Choose one favourite artwork <br>in every category</h2>-->
                        <h2 class="welcome-section__subtitle">Contest is over. We will announce the results on <b>December 1</b>.</h2>
                        <div class="welcome-section__timer timer" data-award="Dec 31, 2018 00:00:00">
                        </div>
                    </div>
                </div>

                <div class="scroll-tip d-none d-md-block">
                    Scroll to vote
                </div>
            </div>
        </section>
        <section class="page-wrapper awards-section">
            <div class="page-content">
                <div class="container-fluid content-wrap">
                    <div class="row">
                        <div class="col-12 col-sm-10 col-md-8">
                            <div class="content">
                                <h3>We invite you all to take a part in the annual 80 Level Awards</h3>
                                <p>Artists have been working hard the whole year creating outstanding works and sharing their experience and knowledge with the community. </p>
                                <p>80 Level Awards is a way to say thank you to all those talents who presented outstanding environments, set up mind-blowing simulations, built graphs for intricate materials, modeled detailed props, and sculpted beautiful characters.</p>
                                <p>You pick one favorite in each category and we’ll define the best of the best in a month! The winners, defined by the community, will get some cool prizes!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="decoration d-none d-md-inline-block">
                    <img src="/pics/rock-and-roll.svg" alt="">
                </div>
            </div>
        </section>
        <section class="page-wrapper awards-section awards-section_gallery">
            <div class="page-content gallery-page">
                <div class="gallery container-fluid clear-intend-right">
					<? for($i=1; $i<=20; $i++){
						$j = $i % 7 +1;
						?>
                        <a href="#" class="gallery__item card-art <?=$j == 2 ? 'card-art_active': ''?>" style="background-image: url('/templates/pics/cards/<?=$j?>.png')">
                            <div class="card-art__inner">
                                <span class="card-art__title">Sculpting a Stylized Girl in ZBrush</span>
                                <span class="card-art__subtitle">Zuzana Bubenová</span>
                            </div>
                            <span class="card-art__rectangles"></span>
                            <div class="card-art__chosen">
                                <div class="card-art__chosen-wrap">
                                    <span>This is my choice</span>
                                </div>
                            </div>
                        </a>
					<?};?>
                </div>
            </div>
        </section>
        <section class="page-wrapper awards-section">
            <div class="page-content">
                <div class="container-fluid content-wrap">
                    <div class="row">
                        <div class="col">
                            <div class="content">
                                <h1>your choice</h1>
                                <h2>Thanks for participation. We hope you had fun. We will announce the results on <b>October 1.</b></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gallery gallery_limit container-fluid ">
					<? for($i=1; $i<=5; $i++){?>
                    <a href="#" class="gallery__item card-art" style="background-image: url('/templates/pics/cards/1.png')">
                        <div class="card-art__inner">
                            <span class="card-art__title">Sculpting a Stylized Girl in ZBrush</span>
                            <span class="card-art__subtitle">Zuzana Bubenová</span>
                        </div>
                        <span class="card-art__rectangles"></span>
                        <div class="card-art__chosen">
                            <div class="card-art__chosen-wrap">
                                <span>This is my choice</span>
                            </div>
                        </div>
                    </a>
                    <?};?>
                </div>
            </div>
			<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/footer.php";?>
        </section>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/end.php";?>

