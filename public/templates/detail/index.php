<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/meta.php";?>

<div class="page-wrapper">
	<?php include $_SERVER['DOCUMENT_ROOT']."/templates/partial/sidebar.php";?>

	<?php include $_SERVER['DOCUMENT_ROOT']."/templates/partial/modal.php";?>

    <div class="page-wrapper awards-section">
        <div class="page-content page-detail">
            <div class="art-panel">
                <div class="art-panel__body">
                    <div class="gallery-slider">
                        <div class="slider gallery-slider__main">
                            <div class="slider__wrapper">
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-1.png')"></div>
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-2.png')"></div>
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-3.png')"></div>
                            </div>
                            <div class="slider__pagination"></div>
                        </div>
                        <div class="slider gallery-slider__thumbs d-none d-sm-block">
                            <div class="slider__wrapper">
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-1.png')"></div>
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-2.png')"></div>
                                <div class="slider__slide" style="background-image:url('/templates/pics/gallery/img-3.png')"></div>
                            </div>
                        </div>
                    </div>
                    <div class="art-panel__vendors">
                        <span>Vendors:</span> Allegorithmic, 3DS Max, ZBrush, Marmoset
                    </div>
                    <div class="art-panel__beauty d-none d-sm-inline-block">
                        <div class="ribbon">
                            <span class="ribbon__text">1<sup>st</sup> place</span>
                        </div>
                    </div>
                </div>
                <div class="art-info art-panel__footer">
                    <div class="art-info__description">
                        <h4>Production of Physical 3D Figurines in Japan</h4>
                        <div class="d-flex">
                            <ul class="social-list">
                                <li class="social-list__item"><a href="#"><span class="icon-social icon-social_triangle"></span></a></li>
                                <li class="social-list__item"><a href="#"><span class="icon-social icon-social_instagram"></span></a></li>
                                <li class="social-list__item"><a href="#"><span class="icon-social icon-social_fb"></span></a></li>
                                <li class="social-list__item"><a href="#"><span class="icon-social icon-social_twitter"></span></a></li>
                                <li class="social-list__item"><a href="#"><span class="icon-social icon-social_in"></span></a></li>
                            </ul>
                            <span class="art-info__author">Marcos Geymonat</span>
                        </div>
                    </div>
                    <div class="art-info__interaction">
                        <div class="art-info__btn-wrap">
                            <a href="#" class="btn btn_dark btn_tight btn_active art-info__btn-choose">
                               <svg version="1.1" class="btn__icon icon icon-goblet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                  viewBox="0 0 73 72" enable-background="new 0 0 73 72" xml:space="preserve">
<path fill="#E4C291" class="icon-svg" d="M69.3,7.2H58.5V3.6c0-2-1.6-3.6-3.6-3.6h-36c-2,0-3.6,1.6-3.6,3.6l0,3.6H4.5c-2,0-3.6,1.6-3.6,3.6v10.8
	C0.9,29.5,7.3,36,15.3,36h0.2c1.6,9.1,8.7,16.2,17.8,17.7v11.1H22.5V72h28.8v-7.2H40.5V53.7c9-1.5,16.1-8.7,17.7-17.7h0.3
	c7.9,0,14.4-6.5,14.4-14.4V10.8C72.9,8.8,71.3,7.2,69.3,7.2z M8.1,21.6v-7.2h7.2l-0.1,14.4C11.3,28.7,8.1,25.5,8.1,21.6z M46.5,36
	l-9.6-6.4L27.3,36l3.2-11.2l-8-6.8l9.6-1.2l4.8-9.6l4.8,9.6l9.6,1.2l-8,6.8L46.5,36z M58.5,28.8V14.4h7.2v7.2
	C65.7,25.6,62.4,28.8,58.5,28.8z"/>
</svg>
                                <span class="btn__text">This is my choice</span>
                            </a>
                            <div class="art-info__btn-row">
                                <a href="#" class="btn btn_dark btn_tight">
                                    <span class="icon icon-fb icon-fb_bigger"></span>
                                </a>
                                <a href="#" class="btn btn_dark btn_tight">
                                    <span class="icon icon-twitter icon-twitter_bigger"></span>
                                </a>
                                <a href="#" class="btn btn_dark btn_tight">
                                    <span class="icon icon-google icon-google_bigger"></span>
                                </a>
                            </div>
                        </div>
                        <div class="art-info__btn-row art-info__controls">
                            <a href="#" class="btn btn_dark btn_tight btn_expanded art-info__btn-left">
                                <span class="btn__icon icon icon-left"></span><span class="btn__text">Previous work</span>
                            </a>
                            <a href="#" class="btn btn_dark btn_tight btn_expanded art-info__btn-right">
                                <span class="btn__text">Next work</span><span class="btn__icon icon icon-right"></span>
                            </a>
	                        <a href="#" class="btn btn_dark btn_tight btn_expanded art-info__btn-cross">
                                <span class="btn__text">Close</span><span class="btn__icon icon icon-cross"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/end.php";?>

