<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/meta.php";?>

<div class="page-wrapper">
	<?php include $_SERVER['DOCUMENT_ROOT']."/templates/partial/sidebar.php";?>

    <div class="fullpage-slider">
        <section class="page-wrapper welcome-section">
            <?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/header.php";?>
            <div class="page-content">
                <div class="slider slider-main">
                    <div class="slider__parallax-bg" style="background-image:url()" data-swiper-parallax="-23%"></div>
                    <div class="slider__wrapper">
                        <div class="slider__slide">
                            <div class="fullscreen-video">
                                <video muted preload="metadata" autoplay loop="loop" class="startVideo" poster="/pics/slides/video-poster.jpg">
                                    <source src="/video/caring_animation.mp4" type='video/mp4'>
                                    <source src="/video/caring_animation.webm" type='video/webm'>
                                </video>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="welcome-section__content container-fluid">
                    <div class="welcome-section__text-faded">80 level</div>
                    <div class="welcome-section__text">
                        <h1 class="welcome-section__title">Awards 2018</h1>
                        <h2 class="welcome-section__subtitle">We are happy to announce our winners!</h2>
                    </div>
                </div>

                <div class="scroll-tip d-none d-md-block">
                    Scroll to seeeeee winnerrrrrrs
                </div>
            </div>
        </section>
        <section class="page-wrapper awards-section">
            <div class="page-content">
                <div class="results content-wrap">
                   <div class="results__wrap">
                       <div class="slider slider-results">
                           <div class="slider__wrapper">
                               <div class="result-card results__item slider__slide">
                                   <div class="result-card__header">
                                       1<sup>st</sup> place
                                   </div>
                                   <a href="#" class="result-card__image" style="background-image: url('/templates/pics/cards/1.png')"></a>
                                   <div class="result-card__footer">
                                       <div class="result-card__title">
                                           <h4>Marcos Geymonat</h4>
                                       </div>
                                       <div class="results-card__author">
                                           <ul class="social-list">
                                               <li class="social-list__item"><a href="#"><span class="icon-social icon-social_instagram"></span></a></li>
                                               <li class="social-list__item"><a href="#"><span class="icon-social icon-social_triangle"></span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                               <div class="result-card results__item slider__slide">
                                   <div class="result-card__header">
                                       2<sup>nd</sup> place
                                   </div>
                                   <a href="#" class="result-card__image" style="background-image: url('/templates/pics/cards/5.png')"></a>
                                   <div class="result-card__footer">
                                       <div class="result-card__title">
                                           <h4>Duard Mostert</h4>
                                       </div>
                                       <div class="results-card__author">
                                           <ul class="social-list">
                                               <li class="social-list__item"><a href="#"><span class="icon-social icon-social_instagram"></span></a></li>
                                               <li class="social-list__item"><a href="#"><span class="icon-social icon-social_triangle"></span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                               <div class="result-card results__item slider__slide">
                                   <div class="result-card__header">
                                       3<sup>rd</sup> place
                                   </div>
                                   <a href="#" class="result-card__image" style="background-image: url('/templates/pics/cards/3.png')"></a>
                                   <div class="result-card__footer">
                                       <div class="result-card__title">
                                           <h4>Alex Senechal</h4>
                                       </div>
                                       <div class="results-card__author">
                                           <ul class="social-list">
                                               <li class="social-list__item"><a href="#"><span class="icon-social icon-social_instagram"></span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>

                    </div>
                </div>
            </div>
			<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/footer.php";?>
        </section>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/end.php";?>

