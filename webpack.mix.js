let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules\/(?!(dom7|swiper)\/).*|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
						options: Config.babel()
					}
				]
			}
		]
	}
});

mix.js('dev/js/main.js', 'public/js')
   .sass('dev/sass/main.scss', 'public/css');

if (mix.inProduction()) {
  mix.version();
}