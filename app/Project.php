<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected static function boot()
    {
        parent::boot();

        if (! \Request::is('admin/*')) {
            static::addGlobalScope('favorites', function ($builder) {
                $builder->with('favorites');
            });
        }
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }

    public function getLink($throw_favorites = null)
	{
		if($throw_favorites){
			return [$this->slug, 'favorited' => 1 ];
		}
		return $this->slug;
	}

	/**
	 * The vendors that belong to the project.
	 */
	public function vendors()
	{
		return $this->belongsToMany('App\Vendor');
	}

    public function favorite()
    {
        $attributes = ['user_id' => auth()->id(), 'category_id' => $this->category_id];
		$favorite = Favorite::where($attributes)->first();

		if ($favorite){
			$favorite->favorited_id = $this->id;
			$favorite->save();
		} else {
			$this->favorites()->create($attributes);
		}

		return Favorite::where('user_id', auth()->id())->count();
    }

    public function isFavorited()
    {
        return !! $this->favorites->where('user_id', auth()->id())->count();
    }

    public function next()
    {
    	if(\Request::has('favorited') && auth()){
			$next = \Auth::user()->favorites
				->where('order', '<', $this->order)
				->first();

			if(!$next){
				$next = \Auth::user()->favorites
					->first();
			}
		}else{
			$next = Project::orderBy('order', 'asc')
				->where('order', '>', $this->order)
				->where('category_id', $this->category_id)
				->where('id', '<>', $this->id)
				->withoutGlobalScopes()
				->first();

			if(!$next){
				$next = Project::orderBy('order', 'asc')->where('category_id', $this->category_id)->withoutGlobalScopes()->first();
			}
		}

        return $next;
    }

    public function previous()
    {
		if(\Request::has('favorited') && auth()){
			$previous = \Auth::user()->favorites
				->where('order', '>', $this->order)
				->last();

			if(!$previous){
				$previous = \Auth::user()->favorites
					->last();
			}
		}else{
			$previous = Project::orderBy('order', 'desc')
				->where('order', '<', $this->order)
				->where('category_id', $this->category_id)
				->where('id', '<>', $this->id)
				->withoutGlobalScopes()
				->first();

			if(!$previous){
				$previous = Project::orderBy('order', 'desc')->where('category_id', $this->category_id)->withoutGlobalScopes()->first();
			}
		}

        return $previous;
    }

    public function getThumbnail($image, $type){
		// We need to get extension type ( .jpeg , .png ...)
		$ext = pathinfo($image, PATHINFO_EXTENSION);

		// We remove extension from file name so we can append thumbnail type
		$name = str_replace_last('.'.$ext, '', $image);

		// We merge original name + type + extension
		return $name.'-'.$type.'.'.$ext;
	}

	public function getVideoSrc(){
		$source = '';
    	if(strpos($this->video, 'vimeo')){
			$source = 'vimeo';
		}elseif (strpos($this->video, 'youtu')){
			$source = 'youtube';
		};

		$url_arr = explode('/', $this->video);
    	$url = array_pop($url_arr);
		return [
			'source' => $source,
			'url' => $url
		];
	}
}
