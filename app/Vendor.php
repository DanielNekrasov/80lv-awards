<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Vendor extends Model
{
	/**
	 * The projects that belong to the vendor.
	 */
	public function projects()
	{
		return $this->belongsToMany('App\Project');
	}
}
