<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    protected $with = ['favorites'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function favorites()
    {
		return $this->belongsToMany(Project::class, 'favorites', 'user_id', 'favorited_id')
			->orderBy('order', 'desc');
    }

    public function hasFavorites()
    {
        return !!$this->favorites->count();
    }
}
