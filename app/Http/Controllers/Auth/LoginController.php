<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

      /**
     * Redirect the user to the facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
		\Request::session()->put('previousUrl', \URL::previous());
        return Socialite::driver('facebook')->redirect();
    }

     /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        if ( \Request::get('error')) {
            return redirect("/");
        } else {
            $facebook_user = Socialite::driver('facebook')->user();

            $user = $this->userFindOrCreate($facebook_user);
            Auth::login($user, true);
			\Request::session()->flash('LoggedIn', true);
			return redirect(\Request::session()->get('previousUrl')."#");
        }
    }

    public function userFindOrCreate($facebook_user)
    {
        $user = User::where('provider_id', $facebook_user->id)->first();

        if(!$user){
            $user = new User;
            $user->name = $facebook_user->getName();
            $user->email = $facebook_user->getEmail();
            $user->provider_id = $facebook_user->getId();
            $user->save();
        }

        return $user;
    }
}
