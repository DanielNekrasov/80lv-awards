<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Project;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Project $project)
    {
		return $project->favorite();
    }

	public function index()
	{
		$data = [];
		$favorites = Favorite::selectRaw('count(*) AS total, favorited_id')
			->groupBy('favorited_id')
			->orderBy('total', 'desc')
			->get();

		foreach ($favorites as $fav_project){
			$project = Project::find($fav_project['favorited_id']);

			if($project){
				$item['id'] = $project->id;
				$item['name'] = $project->name;
				$item['author'] = $project->author;
				$item['category'] = $project->category->name;
				$item['total'] = $fav_project['total'];
				$data[] = $item;
			}
		}

		return view('vendor.voyager.favorites.browse', compact('data'));
	}
}
