<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class HomeController extends Controller
{
	public $favorited_projects = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
//         $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('order', 'asc')->get();
        if(\Auth::check()){
			$this->favorited_projects =  \Auth::user()->favorites;
		}

        return view('home')
            ->with('categories', $categories)
            ->with('favorited_projects', $this->favorited_projects);
    }
}
