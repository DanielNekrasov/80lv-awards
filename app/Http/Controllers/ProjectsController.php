<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

	public function show($slug)
	{
        $categories = Category::withoutGlobalScopes()->orderBy('order', 'asc')->get();
		$project = Project::where('slug', $slug)->first();

		if(!$project){
			abort(404);
		}
		return view('projects.show', ['categories' => $categories, 'project' => $project])
			->with('next', $project->next())
			->with('previous', $project->previous());
	}
}
