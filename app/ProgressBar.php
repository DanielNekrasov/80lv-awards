<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProgressBar extends Model
{
	static $baseDir = '/video';
	static $video_src = [
		'start' => 'walking-in-bar',
		'state' => 'dancing-in-bar',
		'to-state' => 'pushing-in-bar_start'
	];

	public static function getVideo()
	{
        $voted_categories = 0;
        $src = static::$baseDir.'/'.static::$video_src['start'].'.mp4';
		if($user = \Auth::user()){
            $voted_categories = $user->favorites->count();
            if($voted_categories > 0) {
				if(\Request::session()->get('LoggedIn')){
					$src = static::$baseDir . '/' . static::$video_src['to-state'] . '_' . $voted_categories . '.mp4';
				}else{
					$src = static::$baseDir . '/' . static::$video_src['state'] . '_' . $voted_categories . '.mp4';
				}
            }
        }

		return [
			'step' => $voted_categories,
			'src' => $src
		];
	}

}
