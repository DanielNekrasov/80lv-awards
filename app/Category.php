<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected static function boot()
    {
        parent::boot();

        if (! \Request::is('admin/*')) {
            static::addGlobalScope('projects', function ($builder) {
                $builder->with('projects');
            });
        }
    }

    public function projects()
    {
        return $this->hasMany('App\Project')
            ->orderBy('order', 'asc');
    }
}
