<?php

namespace App\Providers;

use App\ProgressBar;
use App\Banner;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Agent\Agent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	if(! \Request::is('admin/*')) {
			\View::composer('*', function ($view){
				$progressBar = ProgressBar::getVideo();

				/* Mobile detect for getting banner*/
				$agent = new Agent();
				$side_banner = [];
				if(!($agent->isMobile() || $agent->isTablet())){
					$side_banner = Banner::where('type', 'side')->inRandomOrder()->first();
				}

				$view->with('progressBar', $progressBar)
					->with('side_banner', $side_banner);
			});

			\View::composer('home', function ($view){
				$agent = new Agent();
				$across_banner = [];
				if(!($agent->isMobile() || $agent->isTablet())){
					$across_banner = Banner::where('type', 'across')->inRandomOrder()->first();
				}
				$view->with('across_banner', $across_banner);
			});

			\View::composer('projects.show', function ($view){
				$agent = new Agent();
				$imgAdapt = ($agent->isMobile() || $agent->isTablet()) ? true : false;
				$view->with('img_adapt', $imgAdapt);
			});
		}
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if($this->app->isLocal()){
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
    }
}
