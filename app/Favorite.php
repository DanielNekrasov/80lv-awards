<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\FavoriteWasCreated;

class Favorite extends Model
{
	protected $events = [
		'created' => FavoriteWasCreated::class
	];

    protected $guarded = [];
}
