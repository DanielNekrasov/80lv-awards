<?php

namespace App\Listeners;

use App\Events\FavoriteWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;
use App\Mail\Thanks;
use App\User;

class SendThanksEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FavoriteWasCreated  $event
     * @return void
     */
    public function handle(FavoriteWasCreated $event)
    {
//		Log::debug($event->favorite->toArray());
		$user = User::find($event->favorite->user_id);
		if($user->favorites->count() == 5) {
			\Mail::to($user)->send(new Thanks($user));
		}
    }
}
