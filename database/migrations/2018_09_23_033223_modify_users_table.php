<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('provider_id')->nullable()->after('email');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable()->change();;
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable(false)->change();;
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable(false)->change();;
        });

        if (Schema::hasColumn('users', 'provider_id')) {
            Schema::table('users', function ($table) {
                $table->dropColumn('provider_id');
            });
        }
    
    }
}
