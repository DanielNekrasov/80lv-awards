class Gallery {
	constructor() {
		this.galleries = $('.gallery').not('.gallery_limit');
		this.emptyCard = $(`<div class="gallery__item card-art card-art_empty">
								<span>Awards<br>2018</span>
							</div>`);
		window.onresize = this.onResize.bind(this);
		this.onInit();
	}

	onInit(){
		this.calculate();
	}

	onResize(){
		this.calculate();
	}

	calculate(){
		const $emptyCard = this.emptyCard;
		if(window.innerWidth < 768){
			this.galleries.each(function () {
				if($(this).children().length === 20){
					$emptyCard.clone().appendTo($(this));
				}
			});
		}else{
			this.galleries.each(function () {
				if($(this).children().length > 20 ){
					$(this).find('.card-art_empty').eq(0).remove();
				}
			})
		}
	}

}

export const gallery = new Gallery();