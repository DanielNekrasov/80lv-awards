class Preloader {
	constructor() {
		this.container = $('.preloader');
		this.line = $('.preloader__full-line');
		this.animationText = $('.transform-effect');
		this.onInit();
	}

	onInit(){
		const $container = this.container;
		$container.addClass('preloader_active');
		this.line.animate({width:'100%'}, 800, ()=> {
		});
	}

	fadeOut() {
		const $container = this.container;
		const animationText = this.animationText;

		$container.removeClass('preloader_active');
		$('.preloader').delay(300).fadeOut();
		animationText.addClass('transform-effect_active');
	}
}

export const preloader = new Preloader();