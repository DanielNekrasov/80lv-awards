window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
}(document, "script", "twitter-wjs"));

$(function () {
	$('.share-button').not('.tw-share').on('click',  function(event) {
		event.preventDefault();

		var $url = $(this).attr('href'),
			$title = 'Share',
			$width = 600,
			$height = 570,
			$left = (screen.width/2)-($width/2),
			$top = (screen.height/2)-($height/2);

		return window.open($url, $title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, ' +
			'scrollbars=no, resizable=yes, copyhistory=no, width='+$width+', height='+$height+', top='+$top+', left='+$left);
	});
});