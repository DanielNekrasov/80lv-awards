require('../libs/swipe-events');

class Controls {
	constructor() {
		this.prev = $('[data-control="prev"]');
		this.next = $('[data-control="next"]');
		this.close = $('[data-control="close"]');
		if(this.prev.length > 0 || this.next.length > 0 || this.close.length > 0){
			$(document).keyup(this.onKeyPressHandler.bind(this));
			if(window.innerWidth < 1440){
                this.onSwipes.call(this);
			}
		}
	}

	onSwipes(){
		const prev = this.prev;
        const next = this.next;

        $('.gallery-slider__main').swipeEvents()
			.bind("swipeDown",  function(event){
                window.location = next.prop('href');
        	})
			.bind("swipeUp", function(event){
                window.location = prev.prop('href');
			});
	}

	onKeyPressHandler(e){
		if (e.keyCode == 37) { // left
			window.location = this.prev.prop('href');
		}

		if (e.keyCode == 39) { // right
			window.location = this.next.prop('href');
		}

		if (e.keyCode == 27) { // esp
			window.location = this.close.prop('href');
		}
	}
}

export const controls = new Controls();