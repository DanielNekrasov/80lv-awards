export default class Modal {
    constructor(selector) {
        this.modal = $(selector);

        // Event listeners
	    this.onInit();

	    $('#modal-close').on('click', () => {
		    this.closeModal();
        });

	    $(document).keyup((e) => {
		    if (e.keyCode == 27) {
			    this.closeModal()
		    }
	    });
    }

    closeModal(){
        this.modal.hide();
    }

    openModal(){
        this.modal.show();
    }

    onInit(){
    	if(!window.sessionStorage.instructionIsShown && this.modal.length > 0){
		    this.openModal();
		    window.sessionStorage.instructionIsShown =  1;
	    }
    }
}