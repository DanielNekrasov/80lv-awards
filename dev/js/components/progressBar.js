class ProgressBar {
	constructor() {
		this.video = document.querySelector(".progress-bar video");
		this.currentStep = $('.progress-bar').data('step');
		this.baseDir = '/video';
		this.videoSrc = {
			move: 'pushing-in-bar',
			finish: 'finish-bike'
		};
	}

	loadVid(vid) {
		vid.load();
	}

	playVid(vid) {
		const promise = vid.play();
		if (promise !== undefined) {
			promise.then(_ => {
				// Autoplay started!
			}).catch(error => {
				// Autoplay was prevented.
				// Show a "Play" button so that user can start playback.
			});
		}
	}

	pauseVid(vid) {
		vid.pause();
	}

	loadNext(finish = false){
		const container = $(".progress-bar__bar");
		const nextVideo = document.createElement('video');
		nextVideo.className = "next";
		nextVideo.muted = true;
		nextVideo.src = this.getVideoSrc(this.currentStep+1, finish);
		container.append(nextVideo);
	}

	getVideoSrc(step, finish){
		if(finish){
			return `${this.baseDir}/${this.videoSrc['finish']}.mp4`;
		}else{
			return `${this.baseDir}/${this.videoSrc['move']}_${step}.mp4`;
		}
	}

	playCurrent(){
        this.playVid(this.video);
	}

	playNext(){
		const video = document.querySelector('.progress-bar video.next');
		video.className = "active";
		this.playVid(video);
	}

}

export const progressBar = new ProgressBar();