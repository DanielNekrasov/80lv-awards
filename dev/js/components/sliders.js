import Swiper from 'swiper';

const galleryThumbs = new Swiper('.gallery-slider__thumbs', {
	containerModifierClass: 'slider_',
	direction: 'vertical',
	slidesPerView: 5,
	freeMode: true,
	preloadImages: false,
	lazy: {
		loadPrevNext: true,
		preloaderClass: "slider__preloader"
	},
	watchSlidesVisibility: true,
	watchSlidesProgress: true,
	clickable: true,
	wrapperClass: 'slider__wrapper',
	slideClass: 'slider__slide',
	thumbsContainerClass: 'slider-thumbs',
	breakpoints: {
		1439.98: {
			direction: 'horizontal',
		}
	}
});

const galleryArt = new Swiper('.gallery-slider__main', {
	containerModifierClass: 'slider_',
	wrapperClass: 'slider__wrapper',
	slideClass: 'slider__slide',
	direction: 'vertical',
	preloadImages: false,
	loop: true,
	speed: 500,
	lazy: {
		loadPrevNext: true,
		preloaderClass: "slider__preloader"
	},
	pagination: {
		el: '.slider__pagination',
		dynamicBullets: true,
		bulletClass: 'slider__bullet',
		bulletActiveClass: 'slider__bullet_active'
	},
	thumbs: {
		swiper: galleryThumbs
	},
	keyboard: {
		enabled: true,
		onlyInViewport: false,
	},
	breakpoints: {
		1439.98: {
			direction: 'horizontal',
		}
	},
	on: {
		init: function () {
			if(window.innerWidth <  768){
				this.thumbs.swiper = false;
			}
			this.mousewheel.enable();
		},
		doubleTap: function () {
			const $formFavorites = $('form#favorites');
			if($formFavorites.length > 0){
				$formFavorites.submit();
			}
		}
	}
});

const resultsSlider = new Swiper('.slider-results', {
	wrapperClass: 'slider__wrapper',
	slideClass: 'slider__slide',
	simulateTouch:false,
	slidesPerView: 3,
	breakpoints: {
		767.98: {
			slidesPerView: 1,
		}
	}
});
