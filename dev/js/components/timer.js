class Timer {
    constructor(){
        if( document.querySelector('.timer')){
            this.onInit();
        }
    }

    onInit(){
        const countDown = document.querySelector('.timer');
        const awardDateString = countDown.getAttribute('data-award');   
        const awardDate = new Date(awardDateString).getTime();
        // Update every second
        const intvl = setInterval(() => {
            // Get todays date and time (ms)
            const now = new Date().getTime();

            // Distance from now to the launch date
            const distance = awardDate - now;
            
            // Time calcucation
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((distance % (1000 * 60)) / (1000));

            countDown.innerHTML = `
                ${days} days <span class="timer__clock">${hours}:${minutes}:${seconds}</span>
            `;

            if(distance < 0) {
                clearInterval(intvl);
            }

        }, 1000);
    }
}

export const timer = new Timer();