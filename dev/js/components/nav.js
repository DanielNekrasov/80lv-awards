window.$ = require('jquery');

export default class Nav {
    constructor(selector) {
        this.navList = $(selector);
        this.navItems = $(`${selector} ${selector}__item`);
        this.navLinks = $(`${selector} a`);
        this.navSize = this.navItems.length;
    }

	activateItem(hash){
        this.navLinks.each(function () {
            let $navItem = $(this).parent();

			if($(this)[0].hash == hash){
				$navItem.addClass('nav-circles__item_active')
            }else {
                $navItem.removeClass('nav-circles__item_active')
                $(this).blur();
            }
        });
	}

	adjustMobileNav(speed){
        if(window.innerWidth >=  768) {
            return false;
        }

		let $activeItem = this.navList.find('.nav-circles__item_active');

		if($activeItem.length > 0 && $activeItem.index() <= this.navSize - 1) {
			this.navList.animate({scrollLeft: $activeItem[0].offsetLeft - 10}, speed );
		}
    }
}