import { progressBar } from "./progressBar";

class Favorites {
	constructor() {
		this.form = $('form#favorites');
		this.form.on('submit', this.onSubmit.bind(this));
	}

	onSubmit(e){
		e.preventDefault();
		const btn = this.form.find('[type="submit"]');
		const token =  this.form.find('[name=_token]').val();

    btn.addClass('btn_active');
    btn.prop('disabled', 'disabled');
    btn.find('.btn__text').text('This is my choice');

		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : token }
		});

		$('.favorite__icon').addClass('favorite__icon_animate');

		$.ajax({
			type: this.form[0].method,
			url: this.form[0].action,

			success: function (data) {
				if(data){
					if(data > progressBar.currentStep){
						progressBar.loadNext();
						progressBar.playNext();
						if(data == 5) {
							setTimeout(function(){
								progressBar.loadNext(true);
								progressBar.playNext();
							}, 3000)
						}
					}else{
						progressBar.playCurrent();
					}
				}
			}
		})
	}
}

export const favorites = new Favorites();