import Swiper from "swiper";

window.$ = window.jQuery = require('jquery');
require('./libs/jquery.onepage-scroll');
require('./components/sliders');
require('./components/share');

import Modal from './components/modal';
import Nav from './components/nav';
import Favorites from './components/favorites';

import { timer } from './components/timer';
import { progressBar } from './components/progressBar';
import { controls } from './components/controls';
import { favorites } from './components/favorites';
import { preloader } from "./components/preloader";
import { gallery } from "./components/gallery";

/* Initialization */
const modal = new Modal('.modal');
const nav = new Nav('.nav-circles');

const $navbar = $('.sidebar');
const $fullPageSlider = $(".fullpage-slider");

/* Main slider */
const mainSlider = new Swiper('.slider-main', {
	containerModifierClass: 'slider_',
	slidesPerView: "auto",
	loop: true,
	init:false,
	loopedSlides:1,
	effect: 'fade',
	fadeEffect: {
		crossFade: true
	},
	speed: 1000,
	slideActiveClass: 'slider__slide_active',
	keyboard: {
		enabled: true,
		onlyInViewport: false,
	},
	wrapperClass: 'slider__wrapper',
	slideClass: 'slider__slide'
});

const video = $('.startVideo');
/* Event Listeners */
$(document).ready(onPageLoad);
$('.categories-page .nav-circles a').on('click', onNavClickHandler);

const preloaderTimeout = setTimeout(() => {
	preloader.fadeOut();
}, 800);

if(video.length > 0){
	video[0].onloadstart = function () {
		clearTimeout(preloaderTimeout);
	};

	video[0].oncanplay = function() {
		setTimeout(() => {
			preloader.fadeOut();
		}, 600)
	};
	video[0].load();
}

/* OnePage scroll */
$fullPageSlider.onepage_scroll({
    sectionContainer: "section",
    easing: "ease",
    animationTime: 1000,
    pagination: false,
    updateURL: false,
    beforeMove: function (index) {
        const sectionAnchor = $fullPageSlider.find(`[data-index="${index}"]`).attr('data-anchor');
        nav.activateItem('#'+sectionAnchor);
        $navbar.removeClass('sidebar_show-silent');
        if(index > 1){
            $navbar.addClass('sidebar_show');
        }else{
            $navbar.removeClass('sidebar_show');
        }
		nav.adjustMobileNav()
    },
    afterMove: function (index) {
        if(index === 2) {
            progressBar.playCurrent();
        }
	    if(index !== 1) {
		    video[0].pause();
	    }else{
		    const promise = video[0].play();
		    if (promise !== undefined) {
			    promise.then(_ => {
			    }).catch(error => {

			    });
		    }
	    }
    },
    loop: false,
    keyboard: true,
    responsiveFallback: false,
    direction: "vertical"
});

/* Event Handlers */
function onPageLoad(){
    if(window.location.hash !== "") {
        const hash = window.location.hash;
        const index = $fullPageSlider.find('[data-anchor="'+hash.replace("#", "")+'"]').data('index');
        nav.activateItem(hash);
		nav.adjustMobileNav(0);
        $fullPageSlider.silentMoveTo(index);
        $navbar.addClass('sidebar_show-silent');
    }
}

function onNavClickHandler(e){
    const $navItem = $(e.target).parents('.nav-circles__item');
    const $navLink = $navItem.find('a');
    const hash = $navLink[0].hash;
	const index = $fullPageSlider.find('[data-anchor="'+hash.replace("#", "")+'"]').data('index');
    nav.activateItem(hash);
	nav.adjustMobileNav(300);
	$fullPageSlider.moveTo(index);
}

$(function () {
    if ($('.page-detail').length) {
		nav.adjustMobileNav(0);
	}
});